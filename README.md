Role Name and Description
=========

pgs-dbaconfigs

This role is responsible for applying some database administrator post configuration to an existing database. This role can be extended whenever a new configuration has to be applied to an existing database

1. Create required roles on database
2. Create monitoring views on database
3. Create template database as template
4. Create application database which is specified
5. Create application database user which is specified

Requirements
------------

Please make sure you've installed the following ansible collection on ansible server

```bash
ansible-galaxy collection install community.postgresql
ansible-galaxy collection install community.general
```

Please make sure you've installed the following pip package on target server

```bash
pip3 install psycopg2-binary
```


Role Variables
--------------

| variableName          | exampleValue | Description                       |
|-----------------------|--------------|-----------------------------------|
| consul_host           | localhost    | Consul Server for Configuration   |
| consul_port           | 8500         | Consul Server Port                |
| consul_scheme         | http         | Consul Access Method              |
| consul_validate_certs | yes          | Consul Validation                 |
| consul_token          | sometoken    | Consul ACL Token                  |
| pw_type               | md5          | PostgreSQL Enc. Type for Database |
| dbname                | demo          | Database Name Will be Deployed |
| app_sec               | randomtext          | Example credential |


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yml

---
- hosts: localhost
  connection: local
  become: true
  roles:
    - role: pgs-dbaconfigs
      vars:
        consul_host: 172.17.0.2
        consul_token: randomtokencomingfromconsul
        pw_type: md5


```

License
-------

BSD

Author Information
------------------

* Hüseyin DEMİR
